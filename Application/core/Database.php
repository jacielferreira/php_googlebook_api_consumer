<?php 

namespace Application\core;

use PDO;

class Database extends PDO
{

    private $DB_USER = 'root';
    private $DB_PASSWORD = '';

    private $connection;

    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=php_book', $this->DB_USER, $this->DB_PASSWORD);
    }

    private function setParameters($stmt, $key, $value)
    {
        $stmt->bindParam($key, $value);
    }

    private function mountQuery($stmt, $parameters)
    {
        foreach( $parameters as $key => $value ) {
            $this->setParameters($stmt, $key, $value);
          }
    }

    public function executeQuery(string $query, array $parameters = [])
    {
        $stmt = $this->connection->prepare($query);
        $this->mountQuery($stmt, $parameters);
        $stmt->execute();
        return $stmt;
  }
    
}
<?php 

use Application\core\Controller;

class User extends Controller
{


    public function index()
    {

        $page = file_get_contents("https://www.googleapis.com/books/v1/volumes?q=intitle:");
        $result = json_decode($page, true);

        if(empty($page)){
            http_response_code(204);
            die('Sem conteúdo.');
        }
        
        $data = $result['items'];

        $this->view('user/index', $data);
    }

    public function search()
    {
        $search = preg_replace('/[ -]+/' , '+' , strtolower($_POST["search"]));
        $page = file_get_contents('https://www.googleapis.com/books/v1/volumes?q='.$search);
        $result = json_decode($page, true);
        $data = $result['items'];

        $livros = $this->model('Livros');
        $favoritos = $livros::findAll();

        $this->view('user/index', $data);
    }

    public function show($id = null)
    {

        $page = file_get_contents('https://www.googleapis.com/books/v1/volumes/'.$id);
        $data = json_decode($page, true);

        

        $this->view('user/show', $data);
    }

    public function add(){
        $request = array(
            'titulo_livro' => $_POST["titulo_livro"],
            'cod_livro' => $_POST["cod_livro"],
            'cover_livro'  => $_POST["cover_livro"],
        );
        $livros = $this->model('Livros');
        $data = $livros::create($request);

        header('Location: /user/favorito');
    }

    public function delete(){
        $request = array(
            'id' => $_POST["id"],
        );
        $livros = $this->model('Livros');
        $data = $livros::delete($request);

        header('Location: /user/favorito');
    }

    public function favorito()
  {

    $livros = $this->model('Livros');
    $data = $livros::findAll();


    $this->view('user/favorito', $data);
  }
}
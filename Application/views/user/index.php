<main>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="#">Buscar Livros Google Books</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Home </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Pesquisa <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/user/favorito" tabindex="-1" >Favoritos</a>
      </li>
    </ul>
    <form action="/user/search" method="post"  class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" name="search"  placeholder="Buscar por..." aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
    </form>
  </div>
</nav>    
<div class="container">
<div class="row mb-2">
   
   <?php foreach($data as $i => $book) { ?> 
    
    <div class="col-md-6">
      <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          <strong class="d-inline-block mb-2 text-primary"><?=$book['volumeInfo']['categories'][0]?></strong>
          <h4 class="mb-0"><?=$book['volumeInfo']['title']; ?></h4>
          <div class="mb-1 text-muted">Publicado em <?=$book['volumeInfo']['publishedDate']?></div>
          <p class="card-text mb-auto"><?=substr($book['volumeInfo']['description'], 0, 90) . '...'?></p>
          <a href="/user/show/<?=$book['id']; ?>" class="stretched-link">Continue lendo</a>
        </div>
        <div class="col-auto d-none d-lg-block">
        <img class="bd-placeholder-img" width="200" height="250" src="<?=$book['volumeInfo']['imageLinks']['smallThumbnail'] ?>" alt="<?=$book['volumeInfo']['title']; ?>">
          <!-- <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"></rect><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg> -->
        </div>
       
      </div>
    </div>
    <?php } ?>

    
   
  </div>
  </div>
</main>
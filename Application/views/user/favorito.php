<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="#">Buscar Livros Google Books</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="/">Home </a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="/user/index">Pesquisa </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Favoritos</a>
      </li>
    </ul>
    <form action="/user/search" method="post"  class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" name="search"  placeholder="Buscar por..." aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
    </form>
  </div>
</nav>
<div class="container">
  <div class="row mb-2">
  <div class="col-md-6">
     <h5>Meus Favoritos </h5>
     <?php count($data) >=1 ? print '<p> Total: '.count($data).'</p>' : print '' ?>
  </div>
      <br />
     <?php if(count($data) >= 1) { ?>
      <table style="width:100%">
  <tr>
    <th><center>Capa</center></th>
    <th><center>Título</center></th>
    <th><center>Ação</center></th>
  </tr>
  <?php foreach($data as $i => $book) { ?>
  <tr>
    <td><center><img class="bd-placeholder-img" width="75" height="98" src="<?=$book['cover_livro']?>"></center></td>
    <td><center><strong><?=$book['titulo_livro']?></strong></center></td>
    <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
  Remover
</button></td>
  </tr>

  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Você está prestes a remover um livro favorito!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Você realmente deseja remover esse livro: <i style="color: red;"><?=$book['titulo_livro']?></i> da sua lista de favoritos?
        <br/>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar ação</button>
        <form action="/user/delete" method="post">
          <input hidden name="id" value="<?=$book['id']; ?>" />
        <button type="submit" role="button" class="btn btn-danger">Remova</button>
        </form>
      </div>
    </div>
  </div>
</div>
  
  

  <?php } ?>
 
</table>
     <?php } else { ?>
      <p>Você ainda não salvou nenhum livro nos favoritos. Para navegar na aplicação use o campo de Pesquisa</p>
     <?php } ?>  
    
  </div>
</div>
</body>
</html>
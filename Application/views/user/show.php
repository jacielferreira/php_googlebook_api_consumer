<main>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <a class="navbar-brand" href="#">Buscar Livros Google Books</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Home </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Pesquisa <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/user/favorito" tabindex="-1" >Favoritos</a>
      </li>
    </ul>
    <form action="/user/search" method="post"  class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" name="search"  placeholder="Buscar por..." aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Pesquisar</button>
    </form>
  </div>
</nav>    
<div class="container">
  <div class="starter-template">
     
    <h1><?=$data['volumeInfo']['title']; ?></h1>
    <p>Categoria: <?=$data['volumeInfo']['categories'][0]?> Publicado em <?=$data['volumeInfo']['publishedDate']?></p>
    <p class="lead"><?=$data['volumeInfo']['description']; ?></p>
    <br/>
    <form action="/user/add" method="post">
    <input hidden name="titulo_livro" value="<?=$data['volumeInfo']['title']; ?>" />
    <input hidden name="cod_livro" value="<?=$data['id']; ?>" />
    <input hidden name="cover_livro" value="<?=$data['volumeInfo']['imageLinks']['smallThumbnail'] ?>" />
      <p><button class="btn btn-danger" type="submit" role="button">Favorite esse livro</button></p>
    </form>
  </div>
  
</div>

</main>
### Sobre a Aplicação

- A Aplicação através da API da Google Books lista livros disponiveis na plataforma. Sendo possivel consultar título, descrição, cover do livro e a data de lançamento, criar lista de favoritos e remover-las. 

- A arquitetura do projeto segue o padrão MVC.
- Para consumo da api, criei um metodo dentro da controller que acessa uma url, que permite ultilizar endpoints para realizar consultas.





### Instalação

- Clone este repositório; 
- Dentro da pasta do projeto, há uma query para criação da tabela no banco mysql;
- Dentro da pasta do projeto, em "Application\core\Database.php" pode-se configurar acesso ao banco de dados;
- Será necessário alterar as variaveis:

    private $DB_USER = '';
    private $DB_PASSWORD = '';

    
- No terminal execute php -S localhost:8080 -t public/ dentro da raiz do projeto;

### Tecnologia usadas

- PHP 7.3.2
- mysql

### Horas usadas no desenvolvimento

 - 04h34

### Facilidades Encontradas

 - Desenvolvimento do sistema O.O usando php

### Dificuldades Encontradas

- A Api Google Books volta muitos livros com dados quebrados;
- Com o costume de usar libs para consumo, inicialmente achei estranho;


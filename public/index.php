<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Googlebook Web APP</title>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
  </head>
  <body>

  <?php
    require '../Application/autoload.php';

    use Application\core\App;
    use Application\core\Controller;

    $app = new App();

  ?>
  <script src="/assets/js/jquery-3.5.1.min.js"></script>
  <script src="/assets/js/bootstrap.bundle.min.js"></script>
  </body>
</html>